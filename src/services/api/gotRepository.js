const getGotData = async function(nbPage) {
    const response = await fetch("https://anapioficeandfire.com/api/characters?page="+nbPage+"&pageSize=50")
    console.log(await response.status)
    if(response.status == 200){
        let data = await response.json()
        // console.dir(await data)
        data = deleteNoName(await data)
        return data
    }
    else{
        new Error(response.statusText)
    }
}

function deleteNoName(data){
    let GotData = [];

    for(let i=0; i<data.length; i++){
        if(data[i].name!=""){ GotData.push(data[i]) }
    }
    return GotData;
}

export default getGotData